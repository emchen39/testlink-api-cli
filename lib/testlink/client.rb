require "testlink/client/version"
require "xmlrpc/client"

module Testlink
  module Client

    def self.new uri, key
      klass = const_get("TestlinkAPIClient")
      klass.new uri, key
    end

    class TestlinkAPIClient
      attr_accessor :uri, :key

      def initialize uri, key
        @key = key
        @uri = get_api_uri uri
      end


      def get_api_uri uri
        uri + "/lib/api/xmlrpc/v1/xmlrpc.php"
      end


      def get_result method, input = {}
        connection = XMLRPC::Client.new_from_uri @uri
        input["devKey"] = @key

        result = connection.call2 method, input
        if result[0]
          if defined? result[1][0] and !result[1][0].nil? and result[1][0].has_key? "message"
            warn result[1][0]["message"]
            false
          else
            result[1]
          end
        else
          raise MethodCallFailed, "Method call to XMLRPC API failed."
        end
      end

      
      def check_dev_key

        get_result "tl.checkDevKey"
      end
      

      def get_latest_build_for_test_plan tplanid

        input = {"testplanid" => tplanid }
        get_result "tl.getLatestBuildForTestPlan", input
      end
      

      def get_last_execution_result_by_tcid tplanid, tcid

        input = { "testplanid" => tplanid,
                  "testcaseid" => tcid }
        get_result "tl.getLastExecutionResult", input
      end


      def get_projects
        
       get_result "tl.getProjects" 
      end


      def get_project_test_plans tprojectid

        input = { "testprojectid" => tprojectid }
        get_result "tl.getProjectTestPlans", input
      end


      def get_project_keywords tprojectid

        input = { "testprojectid" => tprojectid }
        get_result "tl.getProjectKeywords", input
      end


      def get_test_case_keywords tcid = "", tcexternalid = ""

        if tcexternalid.nil?
          input = { "testcaseexternalid" => tcexternalid }
        else
          input = { "testcaseid" => tcid }
        end
        get_result "tl.getTestCaseKeywords", input
      end

      
      def get_test_case_keywords_by_tcid tcid
        get_test_case_keywords tcid
      end

      
      def get_test_case_keywords_by_tcexternalid tcexternalid
        get_test_case_keywords "", tcexternalid
      end


      def get_test_project_by_name tprojectname

        input = { "testprojectname" => tprojectname }
        get_result "tl.getTestProjectByName", input
      end

      
      def get_test_plan_by_name tprojectname, tplanname

        input = { "testprojectname" => tprojectname,
                  "testplanname" => tplanname }
        get_result "tl.getTestPlanByName", input
      end


      def get_test_suites_for_test_plan tplanid

        input = { "testplanid" => tplanid }
        get_result "tl.getTestSuitesForTestPlan", input
      end


      def get_test_cases_for_test_plan tplanid, buildid = nil, platformid = nil, testcaseid = nil, keywordid = nil, keywords = nil, executed = nil, assignedto = nil, executestatus = nil, executiontype = nil, getstepinfo = false, details = nil

        input = Hash.new
        input["testplanid"] = tplanid
        input["buildid"] = buildid unless buildid.nil?
        input["platformid"] = platformid unless platformid.nil?
        input["testcaseid"] = testcaseid unless testcaseid.nil?
        input["keywordid"] = keywordid unless keywordid.nil?
        input["keywords"] = keywords unless keywords.nil?
        input["executed"] = executed unless executed.nil?
        input["assignedto"] = assignedto unless assignedto.nil?
        input["executestatus"] = executestatus unless executestatus.nil?
        input["executiontype"] = executiontype unless executiontype.nil?
        input["getstepinfo"] = getstepinfo unless getstepinfo.nil?
        input["details"] = details unless details.nil?

        get_result "tl.getTestCasesForTestPlan", input
      end

      def get_test_cases_for_test_suite tsuiteid, deep=true, details = ""

        input = { "testsuiteid" => tsuiteid, 
                  "deep" => deep,
                  "details" => details }
        get_result "tl.getTestCasesForTestSuite", input
      end

      def get_test_suites_for_test_suite tsuiteid

        input = { "testsuiteid" => tsuiteid }
        get_result "tl.getTestSuitesForTestSuite", input
      end


      def get_test_case_id_by_name tcasename, tsuitename = "", tprojectname = "", tcasepathname = ""

        input = { "testcasename" => tcasename,
                  "testsuitename" => tsuitename,
                  "testprojectname" => tprojectname,
                  "testcasepathanme" => tcasepathname }
        get_result "tl.getTestCaseIDByName", input
      end


      def get_first_level_test_suites_for_test_project tprojectid

        input = {"testprojectid" => tprojectid }
        get_result "tl.getFirstLevelTestSuitesForTestProject", input
      end


      def get_test_case tcid = nil, tcexternalid = nil

        if tcexternalid.nil?
          input = { "testcaseid" => tcid }
        else
          input = { "testcaseexternalid" => tcexternalid }
        end
        get_result "tl.getTestCase", input
      end


      def get_test_case_by_tcid tcid

        get_test_case tcid
      end


      def get_test_case_by_tcexternalid tcexternalid

        get_test_case nil, tcexternalid
      end


      def get_test_suite_by_id tsuiteid

        input =  { "testsuiteid" => tsuiteid }
        get_result "tl.getTestSuiteByID", input
      end


      def get_user_by_id userid

        input =  { "userid" => userid }
        get_result "tl.getUserByID", input
      end


      def update_test_case tcid, version = "", testcasename = "", summary = "", preconditions = "", steps = "", importance = "", executiontype = "", status = "", active = "", estimatedexecduration = "", user = ""

        input = { "testcaseid" => tcid,
                  "version" => version,
                  "testcasename" => testcasename,
                  "summary" => summary,
                  "preconditions" => preconditions,
                  "steps" => steps,
                  "importance" => importance,
                  "executiontype" => executiontype,
                  "status" => status,
                  "active" => active,
                  "estimatedexecduration" => estimatedexecduration,
                  "user" => user }
        get_result "tl.updateTestCase", input
      end


      def set_test_case_execution_type tcid, version, tprojectid, executiontype

       input = { "testcaseid" => tcid,
                 "version" => version,
                 "testprojectid" => tprojectid,
                 "executiontype" => executiontype }
      get_result "tl.setTestCaseExecutionType", input 
      end


      def add_test_case_keywords keywords

        # keywords is a map key: testcaseexternalid, values: array of keyword name
        input = { "keywords" => keywords }
        get_result "tl.addTestCaseKeywords", input
      end


      def remove_test_case_keywords keywords

        input = { "keywords" => keywords }
        get_result "tl.removeTestCaseKeywords", input
      end
    end
  end
end
